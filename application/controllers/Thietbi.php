<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thietbi extends CI_Controller {

	public function index() {
		$data = [
			'mainTitle' => 'Trang chủ',
			'mainContent' => 'trangchu'
		];
		$this->load->view('layoutMain', $data);
	}

	public function danhsach() {
		$data = [
			'mainTitle' => 'Danh sách thiết bị',
			'mainContent' => 'thietbi/danhsach'
		];
		$this->load->view('layoutMain', $data);
	}

	public function lapphieunhap() {
		$data = [
			'mainTitle' => 'Lập phiếu nhập thiết bị',
			'mainContent' => 'thietbi/lapphieunhap'
		];
		$this->load->view('layoutMain', $data);
	}

	public function lapphieuthanhly() {
		$data = [
			'mainTitle' => 'Lập phiếu thanh lý thiết bị',
			'mainContent' => 'thietbi/lapphieuthanhly'
		];
		$this->load->view('layoutMain', $data);
	}

	public function lichsunhap() {
		$data = [
			'mainTitle' => 'Lịch sử nhập thiết bị',
			'mainContent' => 'thietbi/lichsunhap'
		];
		$this->load->view('layoutMain', $data);
	}

	public function lichsuthanhly() {
		$data = [
			'mainTitle' => 'Lịch sử thanh lý thiết bị',
			'mainContent' => 'thietbi/lichsuthanhly'
		];
		$this->load->view('layoutMain', $data);
	}
}
