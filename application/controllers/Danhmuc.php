<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Danhmuc extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_Hethong');
		$this->load->model('Model_Taikhoan');
		$this->load->model('Model_Danhmuc');
	}

	public function index() {
		$this->thietbi();
	}

	public function giaovien() {
		$data = [
			'mainTitle' => 'Quản lý giáo viên',
			'mainContent' => 'danhmuc/giaovien'
		];

		$modelDanhmuc = $this->Model_Danhmuc;
		$listGV = $modelDanhmuc->listGV();
		$tmpListGV = $listGV;
		foreach ($listGV as $k => $item) {
			$tmpListGV[$k]['BoMon'] = getBoMonById($item['MaBM']);
		}
		$listGV = $tmpListGV;
		$data['listGV'] = $listGV;

		$this->load->view('layoutMain', $data);
	}

	public function xulyTaoMaDangNhapGV() {
		$hotenGV = $this->input->post('HoTenGV');
		$modelDanhmuc = $this->Model_Danhmuc;
		$modelTaiKhoan = $this->Model_Taikhoan;

		if (empty($hotenGV)) {
			$result = ['status' => false, 'message' => 'Chưa nhập họ tên giáo viên.'];
		} else {
			$newMDN = hoTen2MaDN($hotenGV);
			$tmpMDN = $newMDN;
			$ii = 1;
			do {
				$ex = false;
				if ($modelTaiKhoan->existed($tmpMDN)) {
					$tmpMDN = $newMDN.'.'.$ii;
					$ii++;
					$ex = true;
				}
			} while ($ex);
			$newMDN = $tmpMDN;
			$result = ['status' => true, 'message' => $newMDN];
		}

		echo json_encode($result);
		die;
	}

	public function themgiaovien() {

		$data = [
			'mainTitle' => 'Thêm giáo viên',
			'mainContent' => 'danhmuc/themgiaovien'
		];

		$modelDanhmuc = $this->Model_Danhmuc;
		$modelTaiKhoan = $this->Model_Taikhoan;
		$newMaGV = '';

		if ($this->input->post('do')) {
			$params = $this->input->post();
			unset($params['do']);
			$newMaGV = $params['MaGV'];
			if (empty(trim($params['MaGV'])) || empty(trim($params['HoTenGV'])) || empty(trim($params['MaDangNhap'])) || empty(trim($params['Email'])) || empty(trim($params['DienThoai']))) {
				$data['error'] = 'Chưa nhập đầy đủ thông tin';
			} elseif ( ! filter_var($params['Email'], FILTER_VALIDATE_EMAIL) ) {
				$data['error'] = 'Email không hợp lệ';
			} elseif ( ! preg_match("/^[0][0-9]{9,10}$/", $params['DienThoai']) ) {
				$data['error'] = 'Điện thoại không hợp lệ';
			} elseif ( $modelDanhmuc->existedByEmail($params['Email']) ) {
				$data['error'] = 'Email đã tồn tại trong hệ thống';
			} elseif ( $modelDanhmuc->existedByDienThoai($params['DienThoai']) ) {
				$data['error'] = 'Điện thoại đã tồn tại trong hệ thống';
			} else {
				if ($modelTaiKhoan->addTaiKhoan($params['MaDangNhap'], $params['DienThoai'])) {
					if ($modelDanhmuc->addGV($params)) {
						redirect(base_url('danhmuc/giaovien'));
						die;
					} else {
						$data['error'] = 'Không thể thêm dữ liệu giáo viên';
					}
				} else {
					$data['error'] = 'Không thể thêm tài khoản đăng nhập cho giáo viên';
				}
			}

			$data['params'] = $params;
		}

		if (empty($newMaGV)) {
			$listGV = $modelDanhmuc->listGV();
			
			if (!$listGV) {
				$newMaGV = 'GV001';
			} else {
				$max = 0;
				foreach ($listGV as $item) {
					$maxMaGV = (int) substr($item['MaGV'], 2, 3);
					if ($maxMaGV > $max) $max = $maxMaGV;
				}
				$max += 1;
				$newMaGV = newMaGV($max);
			}
		}

		$listBoMon = $modelDanhmuc->listBoMon();
		$data['listBoMon'] = $listBoMon;
		$data['newMaGV'] = $newMaGV;

		$this->load->view('layoutMain', $data);
	}

	public function taikhoan() {

		$modelTaiKhoan = $this->Model_Taikhoan;

		$listTaiKhoan = $modelTaiKhoan->listTaiKhoan();
		foreach ($listTaiKhoan as $k => $item) {
			$listTaiKhoan[$k]['order'] = $k+1;
			$listTaiKhoan[$k]['LoaiTK'] = $item['PhanQuyen'] == 1 ? '<span class="badge badge-success">Giáo viên</span>' : '<span class="badge badge-important">Quản lý</span>';
			$listTaiKhoan[$k]['GiaoVien'] = $item['PhanQuyen'] == 1 ? getGVByMaDN($item['MaDangNhap']) : '-';
			$listTaiKhoan[$k]['TrangThai'] = $item['TrangThai'] == 1 ? '<span class="badge badge-success">Đang hoạt động</span>' : '<span class="badge badge-important">Tạm khóa</span>';
		}

		$data = [
			'mainTitle' => 'Quản lý tài khoản',
			'mainContent' => 'danhmuc/taikhoan',
			'listTaiKhoan' => $listTaiKhoan
		];
		$this->load->view('layoutMain', $data);
	}

	public function lop() {

		$modelDanhmuc = $this->Model_Danhmuc;
		
		$error = $lop = $khoi = '';
		
		if ($this->input->post('do')) {
			$lop = $this->input->post('TenLopMoi');
			$khoi = $this->input->post('Khoi');
			if (!empty($lop)) {
				if($modelDanhmuc->existedLop($lop)) $error = 'Tên lớp đã có trong hệ thống';
				else {
					if ($modelDanhmuc->addLop($lop, $khoi)) {
						header('Location: ' . base_url('danhmuc/lop'));
						die;
					} else {
						$error = 'Lỗi khi thêm lớp mới';
					}
				}
			} else {
				$error = 'Chưa nhập tên lớp';
			}
		}
		$listKhoiLop = listKhoiLop();
		$listLop = $modelDanhmuc->listLop();

		foreach ($listLop as $k => $item) {
			$listLop[$k]['order'] = $k+1;
			$listLop[$k]['khoi'] = $listKhoiLop[$item['KhoiLop']];
		}

		$data = [
			'mainTitle' => 'Quản lý lớp học',
			'mainContent' => 'danhmuc/lop',
			'listLop' => $listLop,
			'lop' => $lop,
			'error' => $error,
			'listKhoiLop' => $listKhoiLop,
			'khoi' => $khoi
		];
		$this->load->view('layoutMain', $data);
	}

	public function xulyUpdateLop() {
		$params = $this->input->post();
		$modelDanhmuc = $this->Model_Danhmuc;

		if (empty($params['TenLop'])) {
			$result = ['status' => false, 'message' => 'Chưa nhập tên lớp.'];
		} else {

			if ($modelDanhmuc->updateLop($params)) {
				$result = ['status' => true, 'message' => 'Cập nhật thành công.'];
			} else {
				$result = ['status' => false, 'message' => 'Đã có lỗi xảy ra.'];
			}
		}

		echo json_encode($result);
		die;
	}

	function xulyXoaLop() {
		
		if ($this->session->userdata('PhanQuyen') < 2) {
			die('Not allow!');
		}

		$modelDanhmuc = $this->Model_Danhmuc;
		$malop = $this->input->post('MaLop');
		
		if (!$modelDanhmuc->existedLopById($malop)) {
			$result = array(
				'status' => false,
				'message' => 'Mã lớp này không tồn tại. Vui lòng kiểm tra lại.'
			);
		} else {
			if ($modelDanhmuc->delLop($malop)) {
				$result = array(
					'status' => true,
					'message' => 'Đã xóa lớp thành công'
				);
			} else {
				$result = array(
					'status' => false,
					'message' => 'Lỗi khi xóa lớp'
				);
			}
		}

		echo json_encode($result);
		die;
	}

	public function bomon() {

		$modelDanhmuc = $this->Model_Danhmuc;
		
		$error = $bomon = '';
		
		if ($this->input->post('do')) {
			$bomon = $this->input->post('BoMonMoi');
			if (!empty($bomon)) {
				if($modelDanhmuc->existedBoMon($bomon)) $error = 'Tên bộ môn đã có trong hệ thống';
				else {
					if ($modelDanhmuc->addBoMon($bomon)) {
						header('Location: ' . base_url('danhmuc/bomon'));
						die;
					} else {
						$error = 'Lỗi khi thêm bộ môn mới';
					}
				}
			} else {
				$error = 'Chưa nhập tên bộ môn';
			}
		}

		$listBoMon = $modelDanhmuc->listBoMon();

		foreach ($listBoMon as $k => $item) {
			$listBoMon[$k]['order'] = $k+1;
			$listBoMon[$k]['SLGiaoVien'] = numGVInBoMon($item['MaBM']);
		}

		$data = [
			'mainTitle' => 'Tổ bộ môn',
			'mainContent' => 'danhmuc/bomon',
			'listBoMon' => $listBoMon,
			'error' => $error,
			'bomon' => $bomon
		];
		$this->load->view('layoutMain', $data);
	}

	public function xulyDoiTenBM() {

		if ($this->session->userdata('PhanQuyen') < 2) {
			die('Not allow!');
		}

		$params = $this->input->post();
		$modelDanhmuc = $this->Model_Danhmuc;

		if (empty($params['MaBM'])) {
			$result = ['status' => false, 'message' => 'Mã bộ môn không hợp lệ.'];
		} elseif (empty($params['TenBM'])) {
			$result = ['status' => false, 'message' => 'Tên bộ môn không thể để trống.'];
		} else {
			$data = [
				'MaBM' => $params['MaBM'],
				'TenBM' => $params['TenBM']
			];

			if ($modelDanhmuc->updateBoMon($data)) {
				$result = ['status' => true, 'message' => 'Đổi tên bộ môn thành công.'];
			} else {
				$result = ['status' => false, 'message' => 'Đã có lỗi xảy ra.'];
			}
		}

		echo json_encode($result);
		die;
	}

	function xulyXoaBM() {
		
		if ($this->session->userdata('PhanQuyen') < 2) {
			die('Not allow!');
		}

		$modelDanhmuc = $this->Model_Danhmuc;
		$mabm = $this->input->post('MaBM');
		
		if (!$modelDanhmuc->existedBoMonById($mabm)) {
			$result = array(
				'status' => false,
				'message' => 'Mã bộ môn này không tồn tại. Vui lòng kiểm tra lại.'
			);
		} else {
			if ($modelDanhmuc->delBoMon($mabm)) {
				$result = array(
					'status' => true,
					'message' => 'Đã xóa bộ môn thành công'
				);
			} else {
				$result = array(
					'status' => false,
					'message' => 'Lỗi khi xóa bộ môn'
				);
			}
		}

		echo json_encode($result);
		die;
	}

	public function phong() {

		$modelDanhmuc = $this->Model_Danhmuc;
		
		$error = $phong = $loai = '';
		
		if ($this->input->post('do')) {
			$phong = $this->input->post('TenPhongMoi');
			$loai = $this->input->post('LoaiPhong');
			if (!empty($phong)) {
				if($modelDanhmuc->existedPhong($phong)) $error = 'Tên phòng đã có trong hệ thống';
				else {
					if ($modelDanhmuc->addPhong($phong, $loai)) {
						header('Location: ' . base_url('danhmuc/phong'));
						die;
					} else {
						$error = 'Lỗi khi thêm phòng mới';
					}
				}
			} else {
				$error = 'Chưa nhập tên phòng';
			}
		}

		$listPhong = $modelDanhmuc->listPhong();
		$loaiPhong = listLoaiPhong();

		foreach ($listPhong as $k => $item) {
			$listPhong[$k]['order'] = $k+1;
			$listPhong[$k]['loai'] = $loaiPhong[$item['LoaiPhong']];
		}

		$data = [
			'mainTitle' => 'Quản lý phòng chức năng',
			'mainContent' => 'danhmuc/phong',
			'loaiPhong' => $loaiPhong,
			'phong' => $phong,
			'error' => $error,
			'loai' => $loai,
			'listPhong' => $listPhong
		];
		$this->load->view('layoutMain', $data);
	}

	public function xulyUpdatePhong() {
		$params = $this->input->post();
		$modelDanhmuc = $this->Model_Danhmuc;

		if (empty($params['TenPhong'])) {
			$result = ['status' => false, 'message' => 'Chưa nhập tên phòng.'];
		} else {

			if ($modelDanhmuc->updatePhong($params)) {
				$result = ['status' => true, 'message' => 'Cập nhật thành công.'];
			} else {
				$result = ['status' => false, 'message' => 'Đã có lỗi xảy ra.'];
			}
		}

		echo json_encode($result);
		die;
	}

	function xulyXoaPhong() {

		$modelDanhmuc = $this->Model_Danhmuc;
		$maphong = $this->input->post('MaPhong');
		
		if (!$modelDanhmuc->existedPhongById($maphong)) {
			$result = array(
				'status' => false,
				'message' => 'Phòng này không tồn tại. Vui lòng kiểm tra lại.'
			);
		} else {
			if ($modelDanhmuc->delPhong($maphong)) {
				$result = array(
					'status' => true,
					'message' => 'Đã xóa phòng thành công'
				);
			} else {
				$result = array(
					'status' => false,
					'message' => 'Lỗi khi xóa phòng'
				);
			}
		}

		echo json_encode($result);
		die;
	}

	public function thietbi() {
		$data = [
			'mainTitle' => 'Danh mục thiết bị',
			'mainContent' => 'danhmuc/thietbi'
		];
		$this->load->view('layoutMain', $data);
	}
}
