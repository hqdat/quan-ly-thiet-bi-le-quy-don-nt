<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hethong extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_Hethong');
	}

	public function index() {
		$data = [
			'mainTitle' => 'Trang chủ',
			'mainContent' => 'trangchu'
		];
		$this->load->view('layoutMain', $data);
	}

	public function namhoc() {
		
		$modelHeThong = $this->Model_Hethong;
		$listNamHoc = $modelHeThong->listNamHoc();

		$error = $namhocmoi = '';

		if ($this->input->post('do')) {
			$namhocmoi = $this->input->post('NamHocMoi');
			if (!empty($namhocmoi)) {
				if($modelHeThong->existedNamHoc($namhocmoi)) $error = 'Năm học đã có trong hệ thống';
				else {
					// Xử lý
					if (strpos($namhocmoi, '-') === false) {
					    $error = 'Nhập chưa đúng định dạng. Ví dụ: <strong>2017 - 2018</strong>';
					} else {
						$arrYear = explode('-', $namhocmoi);
						if (count($arrYear) > 2 || (int) $arrYear[0] < 2000 || (int) $arrYear[0] > 2500 || (int) $arrYear[1] < 2000 || (int) $arrYear[1] > 2500) {
							$error = 'Dữ liệu không hợp lệ.';
						} else {
							if (((int) $arrYear[1] - (int) $arrYear[0]) > 1) {
								$error = 'Không thể cách nhau 2 năm.';
							} else {
								$namhocmoi = $arrYear[0] . ' - ' . $arrYear[1];
								if ($modelHeThong->addNamHoc($namhocmoi)) {
									header('Location: ' . base_url('hethong/namhoc'));
									die;
								} else {
									$error = 'Lỗi khi thêm năm học';
								}
							}
						}
					}
				}
			} else {
				$error = 'Chưa nhập năm học';
			}
		}

		foreach ($listNamHoc as $i => $item) {
			$listNamHoc[$i]['order'] = $i+1;
			$listNamHoc[$i]['NHHienTai'] = $item['NHHienTai'] ? '<span class="badge badge-success">Năm học hiện tại</span>' : '';
			$listNamHoc[$i]['btnDelete'] = $item['NHHienTai'] ? '' : '<a href="javascript:void(0)" class="btn btn-danger btn-mini" onclick="alert(\'Tính năng này đang tạm khóa.\');return false;"><em class="fa fa-trash"> </em> Xóa năm học & toàn bộ dữ liệu</a>';
		}

		$data = [
			'mainTitle' => 'Quản lý năm học',
			'mainContent' => 'hethong/namhoc',
			'listNamHoc' => $listNamHoc,
			'error' => $error,
			'namhocmoi' => $namhocmoi
		];
		$this->load->view('layoutMain', $data);
	}

	public function cauhinh() {
		$data = [
			'mainTitle' => 'Cấu hình hệ thống',
			'mainContent' => 'hethong/cauhinh'
		];
		$this->load->view('layoutMain', $data);
	}
}
