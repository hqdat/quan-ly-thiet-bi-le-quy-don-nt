<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muontra extends CI_Controller {

	public function index() {
		$data = [
			'mainTitle' => 'Trang chủ',
			'mainContent' => 'trangchu'
		];
		$this->load->view('layoutMain', $data);
	}

	public function lichsu() {
		$data = [
			'mainTitle' => 'Lịch sử mượn trả thiết bị / phòng chức năng',
			'mainContent' => 'muontra/lichsu'
		];
		$this->load->view('layoutMain', $data);
	}

	public function muonthietbi() {
		$data = [
			'mainTitle' => 'Lập phiếu mượn thiết bị',
			'mainContent' => 'muontra/muonthietbi'
		];
		$this->load->view('layoutMain', $data);
	}

	public function muonphong() {
		$data = [
			'mainTitle' => 'Lập phiếu mượn phòng chức năng',
			'mainContent' => 'muontra/muonphong'
		];
		$this->load->view('layoutMain', $data);
	}
}
