<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Baocao extends CI_Controller {

	public function index() {
		$data = [
			'mainTitle' => 'Trang chủ',
			'mainContent' => 'trangchu'
		];
		$this->load->view('layoutMain', $data);
	}
}
