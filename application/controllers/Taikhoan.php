<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taikhoan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_Hethong');
		$this->load->model('Model_Taikhoan');
	}

	public function index () {
		$this->dangnhap();
	}

	public function xulyThemTaiKhoan() {
		$params = $this->input->post();
		$modelTaiKhoan = $this->Model_Taikhoan;

		if (!isset($params['MaDangNhap']) || empty($params['MaDangNhap'])) {
			$result = array(
				'status' => false,
				'message' => 'Mã đăng nhập không thể trống'
			);
		} elseif (!isset($params['MatKhau']) || empty($params['MatKhau'])) {
			$result = array(
				'status' => false,
				'message' => 'Mật khẩu không thể trống'
			);
		} elseif (!isset($params['NhapLaiMatKhau']) || empty($params['NhapLaiMatKhau'])) {
			$result = array(
				'status' => false,
				'message' => 'Chưa nhập lại mật khẩu'
			);
		} elseif (isset($params['NhapLaiMatKhau']) && isset($params['MatKhau']) && $params['MatKhau'] != $params['NhapLaiMatKhau']) {
			$result = array(
				'status' => false,
				'message' => 'Hai mật khẩu không khớp'
			);
		} else {
			if ($modelTaiKhoan->getTaiKhoan($params['MaDangNhap'])) {
				$result = array(
					'status' => false,
					'message' => 'Mã đăng nhập này đã được dùng.<br> Hãy chọn Mã đăng nhập khác.'
				);
			} else {

				if ($modelTaiKhoan->addTaiKhoan($params['MaDangNhap'], $params['MatKhau'], 2)) {
					$result = array(
						'status' => true,
						'message' => 'Thêm tài khoản thành công'
					);
				} else {
					$result = array(
						'status' => false,
						'message' => 'Lỗi khi thêm tài khoản'
					);
				}
			}
		}

		echo json_encode($result);
	}

	public function xulyDoiMK() {
		$params = $this->input->post();
		$modelTaiKhoan = $this->Model_Taikhoan;

		if (empty($params['MatKhau'])) {
			$result = ['status' => false, 'message' => 'Chưa nhập mật khẩu.'];
		} elseif (empty($params['NhapLaiMatKhau'])) {
			$result = ['status' => false, 'message' => 'Chưa nhập lại mật khẩu.'];
		} elseif ($params['MatKhau'] != $params['NhapLaiMatKhau']) {
			$result = ['status' => false, 'message' => 'Hai mật khẩu mới không khớp.'];
		} else {
			$data = [
				'MaDangNhap' => $params['MaDangNhap'],
				'MatKhau' => $params['MatKhau']
			];

			if ($modelTaiKhoan->updateTaiKhoan($data)) {
				$result = ['status' => true, 'message' => 'Đổi mật khẩu thành công.'];
			} else {
				$result = ['status' => false, 'message' => 'Đã có lỗi xảy ra.'];
			}
		}

		echo json_encode($result);
		die;
	}

	function xulyXoaTK() {
		if ($this->session->userdata('PhanQuyen') < 2) {
			die('Not allow!');
		}

		$modelTaiKhoan = $this->Model_Taikhoan;
		$madn = $this->input->post('MaDangNhap');
		
		if (!$modelTaiKhoan->existed($madn)) {
			$result = array(
				'status' => false,
				'message' => 'Mã đăng nhập này không tồn tại. Vui lòng kiểm tra lại.'
			);
		} else {
			if ($modelTaiKhoan->delTaiKhoan($madn)) {
				$result = array(
					'status' => true,
					'message' => 'Đã xóa tài khoản thành công'
				);
			} else {
				$result = array(
					'status' => false,
					'message' => 'Lỗi khi xóa tài khoản'
				);
			}
		}

		echo json_encode($result);
		die;
	}

	public function dangnhap () {

		$modelHethong = $this->Model_Hethong;
		$modelTaiKhoan = $this->Model_Taikhoan;

		if (empty($modelTaiKhoan->listTaiKhoan())) {
			$modelTaiKhoan->addTaiKhoan('admin', '123456', 2);
		}

		if (empty($modelHethong->listNamHoc())) {
			$modelHethong->addNamHoc(getNHDauTien(), 1);
		}
		
		if ( $this->session->has_userdata('TaiKhoan') ) {
			header("Location: " . base_url());
			die;
		}

		$error = $taikhoan = $matkhau = $namhoc = "";

		if ( $this->input->post('do') ) {
			$taikhoan = $this->input->post('TaiKhoan');
			$matkhau = $this->input->post('MatKhau');
			$namhoc = $this->input->post('NamHoc');
			if ( empty($taikhoan) || empty($matkhau) ) {
				$error = "Chưa nhập đầy đủ thông tin.";
			} else {
				// Kiểm tra thông tin đăng nhập
				$tk = $modelTaiKhoan->getTaiKhoan($taikhoan);
				if ($tk) {
					if (!password_verify($matkhau, $tk['MatKhau'])) {
						$error = "Mật khẩu không đúng.";
					} else {
						if ($tk['TrangThai']) {
							$this->session->set_userdata('TaiKhoan', $taikhoan);
							$this->session->set_userdata('PhanQuyen', $tk['PhanQuyen']);
							$ss_namhoc = $modelHethong->getNamHoc($namhoc);
							$this->session->set_userdata('MaNH', $ss_namhoc['MaNH']);
							$this->session->set_userdata('NamHoc', $ss_namhoc['NamHoc']);
							
							header("Location: " . base_url());
							die;
						} else {
							$error = "Tài khoản đang tạm khóa.";
						}
					}
				} else {
					$error = "Thông tin đăng nhập không chính xác.";
				}
			}
		}
		
		$listNamHoc = $modelHethong->listNamHoc();

		$data = [
			'error' => $error,
			'data_login' => [
				'TaiKhoan' => $taikhoan,
				'MatKhau' => $matkhau,
				'NamHoc' => $namhoc
			],
			'listNamHoc' => $listNamHoc
		];

		$this->load->view('taikhoan/dangnhap', $data);
	}

	public function thoat () {
		$this->session->unset_userdata('TaiKhoan');
		header("Location: " . base_url('taikhoan/dangnhap'));
		die;
	}
}
