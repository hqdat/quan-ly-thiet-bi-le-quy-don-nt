<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Taikhoan extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function existed($madn) {
		if ($this->db->where('MaDangNhap', $madn)->from('taikhoan')->count_all_results() > 0) return true;
		return false;
	}

	public function listTaiKhoan() {
		return $this->db->get('taikhoan')->result_array();
	}

	public function getTaiKhoan($madn = '') {
		if ($madn) {
			$result = $this->db->where('MaDangNhap', $madn)->get('taikhoan')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addTaiKhoan($maDN, $mkhau, $pquyen = 1, $trthai = 1, $gchu = '') {
		if ($maDN && $mkhau) {
			$data = [
				'MaDangNhap' 	=> $maDN,
				'MatKhau' 		=> password_hash($mkhau, PASSWORD_DEFAULT),
				'PhanQuyen' 	=> $pquyen,
				'TrangThai' 	=> $trthai,
				'GhiChu' 		=> $gchu
			];
			return $this->db->insert('taikhoan', $data);
		}
		return false;
	}

	public function updateTaiKhoan($data = array()) {
		if ($data) {
			$data['MatKhau'] = password_hash($data['MatKhau'], PASSWORD_DEFAULT);
			return $this->db->where('MaDangNhap', $data['MaDangNhap'])->update('taikhoan', $data);
		}
		return false;
	}

	public function delTaiKhoan($madn) {
		if ($madn) {
			return $this->db->where('MaDangNhap', $madn)->delete('taikhoan');
		}
		return false;
	}
	
}