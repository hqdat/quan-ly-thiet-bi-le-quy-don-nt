<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Danhmuc extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/**
	 * Bo Mon
	 */
	public function listBoMon() {
		return $this->db->order_by('MaBM', 'DECS')->get('bomon')->result_array();
	}

	public function existedBoMon($bm = '') {
		if (!empty($bm)) {
			if ($this->db->where('TenBM', $bm)->from('bomon')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedBoMonById($id = '') {
		if (!empty($id)) {
			if ($this->db->where('MaBM', $id)->from('bomon')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function getBoMon($id = 0) {
		if ($id) {
			$result = $this->db->where('MaBM', $id)->get('bomon')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addBoMon($bomon = '') {
		if ($bomon) {
			return $this->db->insert('bomon', ['TenBM' => $bomon]);
		}
		return false;
	}

	public function updateBoMon($data = array()) {
		if ($data) {
			return $this->db->where('MaBM', $data['MaBM'])->update('bomon', $data);
		}
		return false;
	}

	public function delBoMon($mabm) {
		if ($mabm) {
			return $this->db->where('MaBM', $mabm)->delete('bomon');
		}
		return false;
	}

	/**
	 * Phong
	 */
	public function listPhong() {
		return $this->db->order_by('MaPhong', 'DECS')->get('phong')->result_array();
	}

	public function existedPhong($name = '') {
		if (!empty($name)) {
			if ($this->db->where('TenPhong', $name)->from('phong')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedPhongById($id = '') {
		if (!empty($id)) {
			if ($this->db->where('MaPhong', $id)->from('phong')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function getPhong($id = 0) {
		if ($id) {
			$result = $this->db->where('MaPhong', $id)->get('phong')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addPhong($name = '', $type = '') {
		if ($name) {
			return $this->db->insert('phong', ['TenPhong' => $name, 'LoaiPhong' => $type]);
		}
		return false;
	}

	public function updatePhong($data = array()) {
		if ($data) {
			return $this->db->where('MaPhong', $data['MaPhong'])->update('phong', $data);
		}
		return false;
	}

	public function delPhong($id) {
		if ($id) {
			return $this->db->where('MaPhong', $id)->delete('phong');
		}
		return false;
	}

	/**
	 * Lop
	 */
	public function listLop() {
		return $this->db->order_by('MaLop', 'DECS')->get('lop')->result_array();
	}

	public function existedLop($name = '') {
		if (!empty($name)) {
			if ($this->db->where('TenLop', $name)->from('lop')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedLopById($id = '') {
		if (!empty($id)) {
			if ($this->db->where('MaLop', $id)->from('lop')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function getLop($id = 0) {
		if ($id) {
			$result = $this->db->where('MaLop', $id)->get('lop')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addLop($lop = '', $khoi = '') {
		if ($lop) {
			return $this->db->insert('lop', ['TenLop' => $lop, 'KhoiLop' => $khoi]);
		}
		return false;
	}

	public function updateLop($data = array()) {
		if ($data) {
			return $this->db->where('MaLop', $data['MaLop'])->update('lop', $data);
		}
		return false;
	}

	public function delLop($id) {
		if ($id) {
			return $this->db->where('MaLop', $id)->delete('lop');
		}
		return false;
	}


	/**
	 * Giao vien
	 */
	public function listGV() {
		return $this->db->get('giaovien')->result_array();
	}

	public function existedByMaGV($maGV = '') {
		if (!empty($maGV)) {
			if ($this->db->where('MaGV', $maGV)->from('giaovien')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedByMaDangNhap($maDN = '') {
		if (!empty($maDN)) {
			if ($this->db->where('MaDangNhap', $maDN)->from('giaovien')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedByEmail($email = '') {
		if (!empty($email)) {
			if ($this->db->where('Email', $email)->from('giaovien')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function existedByDienThoai($dienthoai = '') {
		if (!empty($dienthoai)) {
			if ($this->db->where('DienThoai', $dienthoai)->from('giaovien')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function numGVInBoMon($bm = '') {
		if (!empty($bm)) {
			return $this->db->where('MaBM', $bm)->from('giaovien')->count_all_results();
		}
		return 0;
	}

	public function getByMaGV($maGV = '') {
		if (!empty($maGV)) {
			$result = $this->db->where('MaGV', $maGV)->get('giaovien')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function getGVByMaDN($maDN = '') {
		if (!empty($maDN)) {
			$result = $this->db->where('MaDangNhap', $maDN)->get('giaovien')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addGV($data = null) {
		if ($data) {
			return $this->db->insert('giaovien', $data);
		}
		return false;
	}

	public function updateGV($data = array()) {
		if ($data) {
			return $this->db->where('MaGV', $data['MaGV'])->update('giaovien', $data);
		}
		return false;
	}

	public function delGV($maGV) {
		if ($maGV) {
			return $this->db->where('MaGV', $maGV)->delete('giaovien');
		}
		return false;
	}
	
}