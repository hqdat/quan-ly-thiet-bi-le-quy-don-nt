<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Hethong extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function listNamHoc() {
		return $this->db->order_by('MaNH', 'DECS')->get('namhoc')->result_array();
	}

	public function existedNamHoc($nh = '') {
		if (!empty($nh)) {
			if ($this->db->where('NamHoc', $nh)->from('namhoc')->count_all_results() > 0) return true;
			return false;
		}
		return false;
	}

	public function getNamHoc($id = 0) {
		if ($id) {
			$result = $this->db->where('MaNH', $id)->get('namhoc')->result_array();
			if ($result) return $result[0];
			return false;
		}
		return false;
	}

	public function addNamHoc($namhoc = '', $current = 0) {
		if ($namhoc) {
			return $this->db->insert('namhoc', ['NamHoc' => $namhoc, 'NHHienTai' => $current]);
		}
		return false;
	}
	
}