<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>Danh sách phiếu thanh lý thiết bị</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover hover-pointer data-table">
          <thead>
            <tr>
              <th>Mã phiếu thanh lý</th>
              <th>Ngày thanh lý</th>
              <th>Chứng từ</th>
              <th>Nội dung</th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 10; $i++) { ?>
              <tr class="<?php echo (($i==4) ? 'active' : ''); ?>" onclick="showDetail();return false;">
                <td class="center">PN000<?php echo $i; ?></td>
                <td class="center">01/01/2018</td>
                <td class="center"></td>
                <td>Nhập thiết bị...</td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <div class="widget-box">
        <div class="widget-title">
           <span class="icon"><i class="fa fa-list"></i></span> 
          <h5>Chi tiết phiếu thanh lý: PN00004</h5>
          <div class="buttons">
            <a href="#" class="btn btn-warning btn-mini"><em class="fa fa-pencil"> </em> Cập nhật</a>
            <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
            <a href="javascript:void(0);" class="btn btn-default btn-mini btn-close" onclick="closeDetail();"><em class="fa fa-times"> </em> Đóng</a>
          </div>
        </div>
        <div class="widget-content nopadding">
          <div class="row-fluid">
            <div class="span6">
              <div class="form-horizontal detail-item">
                <div class="control-group">
                  <label class="control-label">Mã phiếu thanh lý:</label>
                  <div class="controls">PN00004</div>
                </div>
                <div class="control-group">
                  <label class="control-label">Ngày thanh lý:</label>
                  <div class="controls">01/01/2018</div>
                </div>
              </div>
            </div>

            <div class="span6">
              <div class="form-horizontal detail-item">
                <div class="control-group">
                  <label class="control-label">Chứng từ:</label>
                  <div class="controls"></div>
                </div>
                <div class="control-group">
                  <label class="control-label">Nội dung:</label>
                  <div class="controls">...</div>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-hover">
            <thead>
              <tr><th colspan="9">DANH SÁCH THIẾT BỊ</th></tr>
              <tr>
                <th>Mã thiết bị</th>
                <th>Tên thiết bị</th>
                <th>Bộ môn</th>
                <th>Đơn vị tính</th>
                <th>Số lượng</th>
                <th>Đơn giá</th>
                <th>Thành tiền</th>
                <th>Xuất xứ</th>
                <th>Ghi chú</th>
              </tr>
            </thead>

            <tbody>
              <?php for ($i=1; $i <= 10; $i++) { ?>
                <tr>
                  <td class="center"><?php echo $i; ?></td>
                  <td>Tên thiết bị <?php echo $i; ?></td>
                  <td class="center">Tên Bộ môn</td>
                  <td class="center">Chiếc</td>
                  <td class="center">5</td>
                  <td class="center">100 000</td>
                  <td class="center">500 000</td>
                  <td class="center">Trung Quốc</td>
                  <td class="center"></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail() {
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").hide();
  }
</script>