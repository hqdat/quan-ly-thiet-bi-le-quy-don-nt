<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">
      <div class="widget-title">
        <span class="icon"><i class="fa fa-info"></i></span> 
        <h5>Thông tin chung</h5>
      </div>
      <div class="widget-content form-horizontal">
      	<div class="row-fluid">
	      	<div class="span4">
	      		<div class="control-group">
              <label class="control-label">Mã phiếu thanh lý:</label>
              <div class="controls"><input type="text" value="PTL00001" readonly="readonly"></div>
            </div>
	      	</div>
	      	<div class="span4">
	      		<div class="control-group">
              <label class="control-label">Ngày thanh lý:</label>
              <div class="controls"><input type="text" data-date="<?php echo date("d/m/Y", time()); ?>" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y", time()); ?>" class="datepicker span10"></div>
            </div>
	      	</div>
	      	<div class="span4">
	      		<div class="control-group">
              <label class="control-label">Năm học:</label>
              <div class="controls">
								<select >
                  <option>2017 - 2018</option>
                  <option>2016 - 2017</option>
                </select>
              </div>
            </div>
	      	</div>
	      </div>
	      <div class="row-fluid">
	      	<div class="span12">
	      		<div class="control-group">
              <label class="control-label">Chứng từ liên quan:</label>
              <div class="controls"><input type="text" class="span12" placeholder="Nhập chứng từ liên quan"></div>
            </div>
          </div>
        </div>
        <div class="row-fluid">
	      	<div class="span12">
	      		<div class="control-group">
              <label class="control-label">Nội dung:</label>
              <div class="controls"><textarea class="span12" rows="3"></textarea></div>
            </div>
	      	</div>
	      </div>
      </div>
		</div>
		
		<hr>

		<div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>Danh sách thiết bị</h5>
        <div class="buttons">
          <a href="#" class="btn btn-success btn-mini"><em class="fa fa-plus"> </em> Thêm</a>
        </div>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Mã thiết bị</th>
              <th>Tên thiết bị</th>
              <th>Bộ môn</th>
              <th>Đơn vị tính</th>
              <th>Số lượng</th>
              <th>Đơn giá</th>
              <th>Thành tiền</th>
              <th>Xuất xứ</th>
              <th>Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 10; $i++) { ?>
              <tr>
                <td class="center"><?php echo $i; ?></td>
                <td>Tên thiết bị <?php echo $i; ?></td>
                <td class="center">Tên Bộ môn</td>
                <td class="center">Chiếc</td>
                <td class="center">5</td>
                <td class="center">100 000</td>
                <td class="center">500 000</td>
                <td class="center">Trung Quốc</td>
                <td class="center">
                  <a href="#" class="btn btn-warning btn-mini"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>

    <hr>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary pull-right"><em class="fa fa-save"> </em> Xong</button>
      <button type="submit" class="btn btn-danger"><em class="fa fa-arrow-left"> </em> Hủy bỏ</button>
    </div>


	</div>
</div>