<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-flag"></i></span> 
        <h5>DANH SÁCH TẤT CẢ THIẾT BỊ</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover hover-pointer data-table">
          <thead>
            <tr>
              <th>Mã thiết bị</th>
              <th>Tên thiết bị</th>
              <th>Bộ môn</th>
              <th>Số lượng</th>
              <th>Hỏng</th>
              <th>Còn dùng được</th>
              <th>Đang mượn</th>
              <th>Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 10; $i++) { ?>
              <tr class="<?php echo (($i==4) ? 'active' : ''); ?>">
                <td class="center"><?php echo $i; ?></td>
                <td>Tên thiết bị <?php echo $i; ?></td>
                <td class="center">Tên Bộ môn</td>
                <td class="center">10</td>
                <td class="center">0</td>
                <td class="center">10</td>
                <td class="center">2</td>
                <td class="center">
                  <a href="#" class="btn btn-info btn-mini"><em class="fa fa-info"> </em> Chi tiết</a>
                  <a href="#" class="btn btn-success btn-mini"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <hr>
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-info"></i></span> 
        <h5>TÊN THIẾT BỊ 4</h5>
      </div>
      <div class="widget-title">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab1">Thông tin chi tiết</a></li>
              <li class=""><a data-toggle="tab" href="#tab2">Lịch sử biến động</a></li>
              <li class=""><a data-toggle="tab" href="#tab3">Lịch sử mượn trả</a></li>
          </ul>
      </div>
      <div class="widget-content tab-content">
          <div id="tab1" class="tab-pane active">
            <div class="row-fluid">
              <div class="span6">
                <div class="form-horizontal detail-item">
                  <div class="control-group">
                    <label class="control-label">Mã thiết bị:</label>
                    <div class="controls">TBHH001</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tên thiết bị:</label>
                    <div class="controls">Tên thiết bị 4</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Quy cách:</label>
                    <div class="controls">Đóng chai</div>
                  </div>
                </div>
              </div>

              <div class="span6">
                <div class="form-horizontal detail-item">
                  <div class="control-group">
                    <label class="control-label">Bộ môn:</label>
                    <div class="controls">Tên Bộ môn</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Số lượng:</label>
                    <div class="controls">10</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Hỏng:</label>
                    <div class="controls">0</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Còn dùng được:</label>
                    <div class="controls">10</div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Đang mượn:</label>
                    <div class="controls">2</div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div id="tab2" class="tab-pane">
            <table class="table table-bordered table-hover data-table">
              <thead>
                <tr>
                  <th>Số chứng từ</th>
                  <th>Ngày chứng từ</th>
                  <th>Loại biến động</th>
                  <th>Số lượng</th>
                  <th>Nội dung</th>
                </tr>
              </thead>

              <tbody>
                <?php for ($i=1; $i <= 4; $i++) { ?>
                  <tr>
                    <td class="center"><?php echo $i; ?></td>
                    <td class="center">01.01.2018</td>
                    <td class="center">Nhập thiết bị</td>
                    <td class="center">10</td>
                    <td>Nhập thiết bị theo phiếu nhập <?php echo $i; ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <div id="tab3" class="tab-pane">
            <table class="table table-bordered table-hover data-table">
              <thead>
                <tr>
                  <th>Mã phiếu mượn</th>
                  <th>Thầy (Cô)</th>
                  <th>Bài học</th>
                  <th>Lớp</th>
                  <th>Số lượng</th>
                  <th>Mượn lúc</th>
                  <th>Trả lúc</th>
                  <th>Ghi chú</th>
                </tr>
              </thead>

              <tbody>
                <?php for ($i=1; $i <= 10; $i++) { ?>
                  <tr class="">
                    <td class="center"><?php echo $i; ?></td>
                    <td>Nguyễn Văn A</td>
                    <td>Tên bài học</td>
                    <td class="text-center">12TL2</td>
                    <td class="center">2</td>
                    <td class="center">9:00 - 01/01/2018</td>
                    <td class="center">9:45 - 01/01/2018</td>
                    <td></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
      </div>                            
    </div>
  </div>
</div>