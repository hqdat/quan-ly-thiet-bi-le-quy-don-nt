<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">
      <div class="widget-title">
        <span class="icon"><i class="fa fa-info"></i></span> 
        <h5>Nhập thông tin phiếu mượn phòng chức năng</h5>
      </div>
      <div class="widget-content form-horizontal">
        <div class="row-fluid">
          <div class="span4">
            <div class="control-group">
              <label class="control-label">Mã phiếu mượn <sub>*</sub></label>
              <div class="controls"><input type="text" value="PM000001" readonly="readonly" class="span12"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label">Ngày mượn <sub>*</sub></label>
              <div class="controls">
                <input type="text" data-date="<?php echo date("d/m/Y", time()); ?>" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y", time()); ?>" class="datepicker span6">
                <div class="time-picker span6">
                  <div class="bootstrap-timepicker">
                    <input type="text" class="timepicker span12">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label">Ngày trả <sub>*</sub></label>
              <div class="controls">
                <input type="text" data-date="<?php echo date("d/m/Y", time()); ?>" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y", time()); ?>" class="datepicker span6">
                <div class="time-picker span6">
                  <div class="bootstrap-timepicker">
                    <input type="text" class="timepicker span12">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      	<div class="row-fluid">
          <div class="span4">
            <div class="control-group">
              <label class="control-label">Bộ môn <sub>*</sub></label>
              <div class="controls">
                <select class="span12">
                  <option>Lý</option>
                  <option>Hóa</option>
                  <option>Sinh</option>
                </select>
              </div>
            </div>
          </div>
	      	<div class="span4">
	      		<div class="control-group">
              <label class="control-label">Giáo viên <sub>*</sub></label>
              <div class="controls">
                <select class="span12">
                  <option>Nguyễn Văn A</option>
                  <option>Nguyễn Văn B</option>
                </select>
              </div>
            </div>
	      	</div>
	      	
	      </div>
        <div class="row-fluid">
          <div class="span8">
            <div class="control-group">
              <label class="control-label">Tên bài học <sub>*</sub></label>
              <div class="controls"><input type="text" class="span12" placeholder="Nhập tên bài học"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label">Lớp <sub>*</sub></label>
              <div class="controls">
                <select class="span12">
                  <option>12TL2</option>
                  <option>12TL3</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row-fluid">
	      	<div class="span12">
	      		<div class="control-group">
              <label class="control-label">Ghi chú</label>
              <div class="controls"><textarea class="span12" rows="3"></textarea></div>
            </div>
	      	</div>
	      </div>

        <hr>
        <div class="row-fluid">
          <div class="span12">
            <div class="control-group">
              <label class="control-label">Chọn phòng <sub>*</sub></label>
              <div class="controls">
                <select class="span4">
                  <option>Phòng thực hành Lý</option>
                  <option>Phòng thực hành Hóa</option>
                  <option>Phòng thực hành Sinh</option>
                  <option>Phòng Giáo án điện tử</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="form-actions" style="margin-left: -15px;margin-right: -15px;margin-bottom: -12px;padding-left: 20px;">
          <button type="submit" class="btn btn-primary pull-right"><em class="fa fa-save"></em>&nbsp;&nbsp;Xong</button>

          <button type="submit" class="btn btn-danger"><em class="fa fa-angle-left"></em>&nbsp;&nbsp;Hủy bỏ</button>
          <button type="submit" class="btn"><em class="fa fa-undo"></em>&nbsp;&nbsp;Xóa hết</button>
        </div>
      </div>
		</div>
	</div>
</div>