<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-flag"></i></span> 
        <h5>Lịch sử</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th>Mã phiếu mượn</th>
              <th>Thầy (Cô)</th>
              <th>Thiết bị / Phòng chức năng</th>
              <th>Bài học</th>
              <th>Lớp</th>
              <th>Mượn lúc</th>
              <th>Trả lúc</th>
              <th>Trạng thái</th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 10; $i++) { ?>
              <tr>
                <td class="center"><?php echo $i; ?></td>
                <td>Nguyễn Văn A</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small><br>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(1)</em></small><br>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(1)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><span class="badge badge-success">Đã trả</span> / <span class="badge badge-important">Chưa trả</span></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>