<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Đăng nhập | Hệ thống quản lý thiết bị Trường THPT Chuyên Lê Quý Đôn</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets');?>/css/maruti-login.css" />
    </head>
    <body>
        <div id="loginbox">
            <img class="logo" src="<?php echo base_url('assets');?>/images/logo.png" alt="Logo Trường">
            <form id="loginform" class="form-vertical" method="post">
		        <div class="control-group normal_text">
                  <h4 style="line-height: 30px;font-size: 20px;">HỆ THỐNG QUẢN LÝ THIẾT BỊ<br> TRƯỜNG THPT CHUYÊN LÊ QUÝ ĐÔN</h4>
                  <select name="NamHoc">
                    <?php foreach ($listNamHoc as $nh): ?>
                        <option value="<?php echo $nh['MaNH']; ?>" <?php echo ($nh['MaNH'] == $data_login['NamHoc'] ? 'selected' : ''); ?>><?php echo $nh['NamHoc']; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="icon-user"></i></span><input type="text" name="TaiKhoan" placeholder="Tên đăng nhập" value="<?php echo $data_login['TaiKhoan']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="icon-lock"></i></span><input type="password" name="MatKhau" placeholder="Mật khẩu" value="<?php echo $data_login['MatKhau']; ?>" />
                        </div>
                    </div>
                </div>
                <?php if ( !empty($error) ) { ?>
                    <div class="alert alert-error" style="width: 70%;margin: 0 auto;">
                        <?php echo $error; ?>
                    </div>
                <?php } ?>
                <div class="form-actions text-center">
                  <input type="submit" name="do" class="btn btn-success" value="Đăng nhập" />
                </div>
            </form>
        </div>
        
        <script src="<?php echo base_url('assets');?>/js/jquery.min.js"></script>  
        <script src="<?php echo base_url('assets');?>/js/maruti.login.js"></script> 
    </body>

</html>
