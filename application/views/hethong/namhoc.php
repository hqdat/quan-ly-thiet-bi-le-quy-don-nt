<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">
      <div class="widget-content nopadding">
        <form action="<?php echo base_url('hethong/namhoc'); ?>" method="post" class="form-horizontal">
          <div class="control-group">
            <label class="control-label">Năm học mới :</label>
            <div class="controls">
              <input type="text" name="NamHocMoi" class="span4" placeholder="Nhập năm học mới" value="<?php echo $namhocmoi; ?>">
              <button type="submit" class="btn btn-success" name="do" value="1"><em class="fa fa-plus"> </em> Thêm</button>
              <?php if (!empty($error)): ?>
                <span class="text-error"><?php echo $error; ?></span>
              <?php endif ?>
            </div>
          </div>
        </form>
      </div>
    </div>
	</div>
</div>

<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Năm học</th>
              <th width="200"></th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listNamHoc as $nh): ?>
              <tr>
                <td class="center"><?php echo $nh['order']; ?></td>
                <td class="center"><?php echo $nh['NamHoc']; ?></td>
                <td class="center">
                  <?php echo $nh['NHHienTai']; ?>
                  <?php echo $nh['btnDelete']; ?>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>