<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $mainTitle; ?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/select2.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/datepicker.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/uniform.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/maruti-style.css" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/maruti-media.css" class="skin-color" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/font-awesome.min.css" class="skin-color" />
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/mystyle.css" />
<script>
  var baseurl = '<?php echo base_url();?>';
</script>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="<?php echo base_url(); ?>">
    <img class="logo" src="<?php echo base_url('assets');?>/images/logo.png" alt="Logo Trường">
    THPT CHUYÊN LÊ QUÝ ĐÔN <small class="hidden-xs"> - QUẢN LÝ THIẾT BỊ</small></a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class="nam-hoc" ><em class="fa fa-calendar"></em> <small class="hidden-xs">Năm học: </small ><span class="text"> <strong><?php echo $this->session->userdata('NamHoc'); ?></strong></span></li>
    <li class="" ><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><?php echo $this->session->userdata('TaiKhoan'); ?></span></a></li>
    <li class=""><a title="" href="<?php echo base_url('taikhoan/thoat'); ?>"><i class="icon icon-share-alt"></i> <span class="text">Thoát</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->

<div id="sidebar"><a href="<?php echo base_url(); ?>" class="visible-phone"><i class="icon icon-home"></i> Trang chủ</a><ul>
    <li class="active"><a href="<?php echo base_url(); ?>"><i class="icon icon-home"></i> <span>Trang chủ</span></a> </li>

    <li><a href="<?php echo base_url('danhmuc/giaovien'); ?>"><i class="icon icon-th"></i> <span>Danh mục</span></a>
      <ul>
        <li><a href="<?php echo base_url('danhmuc/giaovien'); ?>">Giáo viên</a></li>
        <li><a href="<?php echo base_url('danhmuc/taikhoan'); ?>">Tài khoản</a></li>
        <li><a href="<?php echo base_url('danhmuc/lop'); ?>">Lớp</a></li>
        <li><a href="<?php echo base_url('danhmuc/bomon'); ?>">Tổ bộ môn</a></li>
        <li><a href="<?php echo base_url('danhmuc/phong'); ?>">Phòng chức năng</a></li>
      </ul>
    </li>
    <li><a href="<?php echo base_url('hethong/namhoc'); ?>"><i class="icon icon-cog"></i> <span>Hệ thống</span></a>
      <ul>
        <li><a href="<?php echo base_url('hethong/namhoc'); ?>">Quản lý năm học</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="content">
  <div class="container-fluid">
    <?php $this->load->view('partials/header'); ?>
    <?php $this->load->view($mainContent); ?>
  </div>
</div>
</div>
</div>
<div class="row-fluid">
  <div id="footer" class="span12">Bản quyền thuộc về <strong>Trường THPT Chuyên Lê Quý Đôn</strong>. Xây dựng và phát triển bởi <a href="http://quocdatit.com" target="_blank"><strong>Quốc Đạt IT</strong></a></div>
</div>


<script src="<?php echo base_url('assets');?>/js/excanvas.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.ui.custom.js"></script> 
<script src="<?php echo base_url('assets');?>/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.flot.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.flot.resize.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.peity.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/fullcalendar.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/select2.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo base_url('assets');?>/js/bootstrap-timepicker.min.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url('assets');?>/js/jquery.dataTables.min.js"></script>  
<script src="<?php echo base_url('assets');?>/js/maruti.js"></script> 
<script src="<?php echo base_url('assets');?>/js/maruti.tables.js"></script> 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}

$(document).ready(function(){
  
  $('input[type=checkbox],input[type=radio],input[type=file]').uniform();
  
  $('select').select2();

  $('.datepicker').datepicker();

  $('.timepicker').timepicker();
});
</script>
</body>
</html>
