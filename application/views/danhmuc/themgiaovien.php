<div class="row-fluid">
	<div class="span12">
		<a href="<?php echo base_url('danhmuc/giaovien'); ?>" class="btn btn-default"><em class="fa fa-arrow-left"> </em> Hủy bỏ</a>
    <div class="widget-box">
      <div class="widget-content">
        <form action="<?php echo base_url('danhmuc/themgiaovien'); ?>" method="post" class="row-fluid form-horizontal" style="margin-top: 0;">
        <div class="row-fluid">
          <div class="span6">
            <div class="control-group">
              <label class="control-label">Mã giáo viên:</label>
              <div class="controls">
                <input type="text" class="span10" name="MaGV" placeholder="Nhập mã giáo viên" value="<?php echo $newMaGV; ?>" readonly>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Họ tên:</label>
              <div class="controls">
                <input type="text" class="span10" id="HoTenGV" name="HoTenGV" placeholder="Nhập họ tên giáo viên" value="<?php echo (isset($params) ? $params['HoTenGV'] : ''); ?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email:</label>
              <div class="controls">
                <input type="text" class="span10" name="Email" placeholder="Nhập email" value="<?php echo (isset($params) ? $params['Email'] : ''); ?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Điện thoại:</label>
              <div class="controls">
                <input type="text" class="span10" name="DienThoai" placeholder="Nhập số điện thoại" value="<?php echo (isset($params) ? $params['DienThoai'] : ''); ?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Giới tính:</label>
              <div class="controls" style="line-height: 32px;">
                <label style="display: inline-block;">
                  <div class="radio"><input type="radio" name="GioiTinh" value="1" style="opacity: 0;"<?php echo (isset($params) && $params['GioiTinh'] == 0 ? '' : ' checked'); ?>></div>
                  Nam</label>
                <label style="display: inline-block;margin-left: 30px;">
                  <div class="radio"><input type="radio" name="GioiTinh" value="0" style="opacity: 0;"<?php echo (isset($params) && $params['GioiTinh'] == 0 ? ' checked' : ''); ?>></div>
                  Nữ</label>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bộ môn:</label>
              <div class="controls">
                <select class="span8" name="MaBM">
                  <?php foreach ($listBoMon as $item): ?>
                    <option value="<?php echo $item['MaBM']; ?>"<?php echo (isset($params) && $item['MaBM'] == $params['MaBM'] ? ' selected' : ''); ?>><?php echo $item['TenBM']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>
          <div class="span6">
            <div class="control-group">
              <label class="control-label">Tài khoản đăng nhập:</label>
              <div class="controls">
                <input type="text" class="span8" id="MaDangNhap" name="MaDangNhap" placeholder="Chọn nút bên phải để tạo" readonly  value="<?php echo (isset($params) ? $params['MaDangNhap'] : ''); ?>">
                <button onclick="taoMaDN(); return false;" class="btn btn-primary btn-xs">&larr; Tạo từ họ tên</button>
                <span class="help-block" style="margin-top: 5px;"><em>Mật khẩu mặc định là số điện thoại của giáo viên.<!-- <br>Hệ thống sẽ tự động gửi mật khẩu về email giáo viên. --></em></span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Ghi chú:</label>
              <div class="controls">
                <textarea name="GhiChu" class="span12" rows="5"><?php echo (isset($params) ? $params['GhiChu'] : ''); ?></textarea>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="row-fluid">
          <div class="span12 text-center">
            <?php if (!empty($error)): ?>
              <div class="text-error"><?php echo $error; ?></div>
              <br>
            <?php endif ?>
            <button type="submit" class="btn btn-primary" name="do" value="1"><em class="fa fa-save"></em> Thêm</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail() {
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").hide();
  }

  var dangXuLy = false;
  function taoMaDN() {
    if (dangXuLy == false) {
      var hoten = $('#HoTenGV').val();
      if (hoten == '') $('#MaDangNhap').attr('placeholder', 'Chưa nhập họ tên giáo viên');
      else {
        dangXuLy = true;
        $('#MaDangNhap').attr('placeholder', 'Đang tạo, xin chờ...');
        $.ajax({
            url : baseurl + 'danhmuc/xulyTaoMaDangNhapGV',
            type : 'POST',
            data : {HoTenGV : hoten},
            dataType: 'json',
            success : function(res){
              dangXuLy = false;
              $('#MaDangNhap').val(res.message);
            }
        });
      }
    }

  }
</script>