<div class="row-fluid">
	<div class="span12">
    <div class="alert alert-info">
      <strong><em class="fa fa-warning"></em> Lưu ý:</strong> Tài khoản giáo viên được tạo tự động khi thêm mới giáo viên.
    </div>
		<a href="#popupThemTK" data-toggle="modal" class="btn btn-danger"><em class="fa fa-plus"> </em> Thêm tài khoản quản lý</a>
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH TÀI KHOẢN</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th>#</th>
              <th>Tài khoản</th>
              <th>Loại tài khoản</th>
              <th>Tài khoản của giáo viên</th>
              <th>Trạng thái</th>
              <th>Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listTaiKhoan as $item): ?>
              <tr>
                <td class="center"><?php echo $item['order']; ?></td>
                <td><?php echo $item['MaDangNhap']; ?></td>
                <td class="center"><?php echo $item['LoaiTK']; ?></td>
                <td class="center"><?php echo $item['GiaoVien']; ?></td>
                <td class="center"><?php echo $item['TrangThai']; ?></td>
                <td class="center">
                  <a href="#" class="btn btn-success btn-mini" onclick="showDetail('<?php echo $item['MaDangNhap']; ?>');return false;"><em class="fa fa-pencil"> </em> Đổi mật khẩu</a>
                  <?php if ($item['PhanQuyen'] != 1): ?>
                    <a href="javascript:void(0)" onclick="xulyXoaTK('<?php echo $item['MaDangNhap']; ?>');return false;" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
                  <?php endif ?>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <h3>ĐỔI MẬT KHẨU: <span id="maDangNhap_title"></span></h3>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form id="frmDoiMK" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Tài khoản:</label>
              <div class="controls">
                <input type="text" class="span6" name="MaDangNhap" id="maDangNhap_input" value="" readonly>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Mật khẩu mới:</label>
              <div class="controls">
                <input type="password" class="span6" name="MatKhau" id="MatKhauMoi" placeholder="Nhập mật khẩu mới">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Nhập lại mật khẩu mới:</label>
              <div class="controls">
                <input type="password" class="span6" name="NhapLaiMatKhau" id="NhapLaiMatKhau" placeholder="Nhập lại mật khẩu mới">
              </div>
            </div>
            <div class="form-actions">
              <div id="ajaxLoading" class="pull-left ajaxLoading_doimk" style="margin-bottom: 15px;"></div>
              <div class="clearfix"></div>
              <div id="errDoiMK" style="margin-bottom: 15px;"></div>

              <button type="submit" class="btn btn-primary" onclick="xulyDoiMK();return false;"><em class="fa fa-save"> </em> Đổi mật khẩu</button>
              <button class="btn" onclick="closeDetail(); return false;"><em class="fa fa-angle-left"> </em> Hủy bỏ</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

<div id="popupThemTK" class="modal modal-lg hide">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>THÊM TÀI KHOẢN QUẢN LÝ</h3>
  </div>
  <div class="modal-body">
    <form id="frmThemTK" class="row-fluid form-horizontal" style="margin-top: 0;">
      <div class="control-group">
        <label class="control-label">Mã đăng nhập:</label>
        <div class="controls">
          <input type="text" class="span10" name="MaDangNhap" placeholder="Nhập mã đăng nhập">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Mật khẩu mới:</label>
        <div class="controls">
          <input type="password" class="span10" name="MatKhau" placeholder="Nhập mật khẩu mới">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Nhập lại mật khẩu mới:</label>
        <div class="controls">
          <input type="password" class="span10" name="NhapLaiMatKhau" placeholder="Nhập lại mật khẩu mới">
        </div>
      </div>
    </form>
    <div id="ajaxLoading" class="ajaxLoading_them"></div>
    <div id="errThemTK" style="margin-left: 200px;"></div>
  </div>
  <div class="modal-footer">
    <a href="javascript:void(0);" class="btn btn-primary" onclick="xulyThemTK();return false;"><em class="fa fa-save"></em> Thêm</a>
    <a data-dismiss="modal" class="btn" href="#">Hủy</a>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail(madn) {
    $("#maDangNhap_title").html(madn);
    $("#maDangNhap_input").val(madn);
    $("#MatKhauMoi").val('');
    $("#NhapLaiMatKhau").val('');
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").fadeOut(200);
  }

  var dangXuLy = false;
  function xulyThemTK() {
    if (dangXuLy == false) {
      $(".ajaxLoading_them").show();
      dangXuLy = true;
      var frmData = $('#frmThemTK').serialize();

      $.ajax({
          url : baseurl + 'taikhoan/xulyThemTaiKhoan',
          type : 'POST',
          data : frmData,
          dataType: 'json',
          success : function(res){
              $(".ajaxLoading_them").hide();
              dangXuLy = false;
              
              if (res.status == false) {
                  $('#errThemTK').removeClass('text-success').addClass('text-error').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      $('#errThemTK').slideUp(200);
                  }, 3000);
              } else {
                  $('#frmThemTK').slideUp(200);
                  $('#popupThemTK .modal-footer').hide();
                  $('#errThemTK').removeClass('text-error').addClass('text-success').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      location.reload();
                  }, 2000);
              }
          }
      });
    }
  }

  function xulyXoaTK(madn) {
    if (confirm("Bạn có chắc chắn muốn xóa tài khoản: " + madn + " ?")) {
      if (dangXuLy == false) {
        dangXuLy = true;
        $.ajax({
            url : baseurl + 'taikhoan/xulyXoaTK',
            type : 'POST',
            data : {MaDangNhap : madn},
            dataType: 'json',
            success : function(res){
                dangXuLy = false;
                
                if (res.status == false) {
                    alert(res.message);
                } else {
                    alert(res.message);
                    location.reload();
                }
            }
        });
      }
    }
  }

  function xulyDoiMK() {
    if (dangXuLy == false) {
      $(".ajaxLoading_doimk").show();
      dangXuLy = true;
      var frmData = $('#frmDoiMK').serialize();

      $.ajax({
          url : baseurl + 'taikhoan/xulyDoiMK',
          type : 'POST',
          data : frmData,
          dataType: 'json',
          success : function(res){
              $(".ajaxLoading_doimk").hide();
              dangXuLy = false;
              
              if (res.status == false) {
                  $('#errDoiMK').removeClass('text-success').addClass('text-error').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      $('#errDoiMK').slideUp(200);
                  }, 3000);
              } else {
                  $('#errDoiMK').removeClass('text-error').addClass('text-success').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      location.reload();
                  }, 1200);
              }
          }
      });
    }
  }
  
</script>