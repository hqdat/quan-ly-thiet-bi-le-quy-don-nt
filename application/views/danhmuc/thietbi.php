<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-content nopadding">
        <form action="#" method="get" class="form-horizontal">
          <div class="control-group">
            <label class="control-label">Danh mục mới :</label>
            <div class="controls">
              <input type="text" class="span4" placeholder="Nhập tên danh mục mới">
              <button type="submit" class="btn btn-primary"><em class="fa fa-plus"> </em> Thêm</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH DAH MỤC THIẾT BỊ</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th width="100">Mã danh mục</th>
              <th>Tên danh mục</th>
              <th>Số thiết bị</th>
              <th width="200">Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 10; $i++) { ?>
              <tr class="<?php echo (($i==4) ? 'active' : ''); ?>">
                <td class="center"><?php echo $i; ?></td>
                <td class="center">Tên danh mục <?php echo $i; ?></td>
                <td class="center">10</td>
                <td class="center">
                  <a href="#" class="btn btn-success btn-mini" onclick="showDetail();return false;"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <h3>CẬP NHẬT DANH MỤC: Tên danh mục 2</h3>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form action="#" method="get" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Mã dah mục:</label>
              <div class="controls">
                <input type="text" class="span6" value="2" readonly>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Tên danh mục:</label>
              <div class="controls">
                <input type="text" class="span6" placeholder="Nhập tên danh mục">
              </div>
            </div>
            
            <div class="form-actions">
              <button type="submit" class="btn btn-primary"><em class="fa fa-save"> </em> Lưu</button>
              <button type="submit" class="btn"><em class="fa fa-angle-left"> </em> Hủy bỏ</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail() {
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").hide();
  }
</script>