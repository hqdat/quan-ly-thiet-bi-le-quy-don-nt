<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-content nopadding">
        <form action="<?php echo base_url('danhmuc/lop'); ?>" method="post" class="form-horizontal">
          <div class="row-fluid" style="margin: 0;">
            <div class="span3">
              <div class="control-group">
                <label class="control-label">Khối :</label>
                <div class="controls">
                  <select class="span12" name="Khoi">
                    <?php foreach ($listKhoiLop as $k => $item): ?>
                      <option value="<?php echo $k; ?>"<?php echo ($k == $khoi ? ' selected' : ''); ?>><?php echo $item; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="span9">
              <div class="control-group">
                <label class="control-label">Tên lớp mới :</label>
                <div class="controls">
                  <input type="text" name="TenLopMoi" class="span4" placeholder="Nhập tên lớp mới" value="<?php echo $lop; ?>">
                  <button type="submit" class="btn btn-primary" name="do" value="1"><em class="fa fa-plus"> </em> Thêm</button>
                  <?php if (!empty($error)): ?>
                    <span class="text-error"><?php echo $error; ?></span>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH LỚP</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th width="100">#</th>
              <th>Tên lớp</th>
              <th>Khối</th>
              <th width="200">Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listLop as $item): ?>
              <tr>
                <td class="center"><?php echo $item['order']; ?></td>
                <td class="center"><?php echo $item['TenLop']; ?></td>
                <td class="center"><?php echo $item['khoi']; ?></td>
                <td class="center">
                  <a href="#" class="btn btn-success btn-mini" onclick="showDetail(<?php echo $item['MaLop']; ?>,'<?php echo $item['TenLop']; ?>',<?php echo $item['KhoiLop']; ?>);return false;"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini" onclick="xulyXoaLop('<?php echo $item['MaLop']; ?>','<?php echo $item['TenLop']; ?>');return false;"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <h3>CẬP NHẬT LỚP: <span id="update_TenLop"></span></h3>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form id="frmUpdateLop" class="form-horizontal">
            <input type="hidden" name="MaLop" id="update_MaLop">
            <div class="control-group">
              <label class="control-label">Khối</label>
              <div class="controls">
                <select id="update_Khoi" class="span6" name="KhoiLop">
                  <?php foreach ($listKhoiLop as $k => $item): ?>
                    <option value="<?php echo $k; ?>"><?php echo $item; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Tên lớp:</label>
              <div class="controls">
                <input name="TenLop" type="text" id="update_TenLop_input" class="span6" placeholder="Nhập tên lớp">
              </div>
            </div>
            
            <div class="form-actions">
              <div id="ajaxLoading" class="pull-left ajaxloading_update" style="margin-bottom: 15px;"></div>
              <div class="clearfix"></div>
              <div id="errUpdate" style="margin-bottom: 15px;"></div>

              <button type="submit" class="btn btn-primary" onclick="xulyUpdateLop();return false;"><em class="fa fa-save"> </em> Lưu</button>
              <button class="btn" onclick="closeDetail(); return false;"><em class="fa fa-angle-left"> </em> Hủy bỏ</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail(malop, tenlop, khoi) {
    $("#update_TenLop").html(tenlop);
    $("#update_TenLop_input").val(tenlop);
    $("#update_Khoi").val(khoi);
    $("#update_MaLop").val(malop);
    $('select').select2();
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").hide();
  }

  var dangXuLy = false;
  function xulyXoaLop(malop, tenlop) {
    if (confirm("Bạn có chắc chắn muốn xóa lớp: " + tenlop + " ?")) {
      if (dangXuLy == false) {
        dangXuLy = true;
        $.ajax({
            url : baseurl + 'danhmuc/xulyXoaLop',
            type : 'POST',
            data : {MaLop : malop},
            dataType: 'json',
            success : function(res){
                dangXuLy = false;
                
                if (res.status == false) {
                    alert(res.message);
                } else {
                    alert(res.message);
                    location.reload();
                }
            }
        });
      }
    }
  }
  function xulyUpdateLop() {
    if (dangXuLy == false) {
      $(".ajaxloading_update").show();
      dangXuLy = true;
      var frmData = $('#frmUpdateLop').serialize();

      $.ajax({
          url : baseurl + 'danhmuc/xulyUpdateLop',
          type : 'POST',
          data : frmData,
          dataType: 'json',
          success : function(res){
              $(".ajaxloading_update").hide();
              dangXuLy = false;
              
              if (res.status == false) {
                  $('#errUpdate').removeClass('text-success').addClass('text-error').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      $('#errUpdate').slideUp(200);
                  }, 3000);
              } else {
                  $('#errUpdate').removeClass('text-error').addClass('text-success').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      location.reload();
                  }, 1200);
              }
          }
      });
    }
  }
</script>