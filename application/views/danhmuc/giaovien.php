<div class="row-fluid">
	<div class="span12">
		<a href="<?php echo base_url('danhmuc/themgiaovien'); ?>" class="btn btn-primary"><em class="fa fa-plus"> </em> Thêm giáo viên mới</a>
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH TẤT CẢ GIÁO VIÊN</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover hover-pointer data-table">
          <thead>
            <tr>
              <th>Mã giáo viên</th>
              <th>Họ tên</th>
              <th>Bộ môn</th>
              <th>Điện thoại</th>
              <th>Email</th>
              <th>Mã đăng nhập</th>
              <th>Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listGV as $item): ?>
              <tr>
                <td class="center"><?php echo $item['MaGV']; ?></td>
                <td><?php echo $item['HoTenGV']; ?></td>
                <td class="center"><?php echo $item['BoMon']; ?></td>
                <td class="center"><?php echo $item['DienThoai']; ?></td>
                <td class="center"><?php echo $item['Email']; ?></td>
                <td class="center"><?php echo $item['MaDangNhap']; ?></td>
                <td class="center">
                  <!-- <a href="#" class="btn btn-info btn-mini"><em class="fa fa-info"> </em> Chi tiết</a> -->
                  <a href="javascript:void(0)" class="btn btn-success btn-mini" onclick="alert('Tính năng này đang tạm khóa.');return false;"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <!-- <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a> -->
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <div class="widget-box">
        <div class="widget-title">
           <span class="icon"><i class="fa fa-info"></i></span> 
          <h5>Thông tin giáo viên: Họ tên GV 4</h5>
          <div class="buttons">
            <a href="#" class="btn btn-warning btn-mini"><em class="fa fa-pencil"> </em> Cập nhật</a>
            <a href="#" class="btn btn-danger btn-mini"><em class="fa fa-trash"> </em> Xóa</a>
            <a href="javascript:void(0);" class="btn btn-default btn-mini btn-close" onclick="closeDetail();"><em class="fa fa-times"> </em> Đóng</a>
          </div>
        </div>
        <div class="widget-content nopadding">
          <div class="row-fluid">
            <div class="span6">
              <div class="form-horizontal detail-item">
                <div class="control-group">
                  <label class="control-label">Mã giáo viên:</label>
                  <div class="controls">4</div>
                </div>
                <div class="control-group">
                  <label class="control-label">Họ tên:</label>
                  <div class="controls">Họ tên GV 4</div>
                </div>
                <div class="control-group">
                  <label class="control-label">Ngày sinh:</label>
                  <div class="controls">01/01/1970</div>
                </div>
                <div class="control-group">
                  <label class="control-label">Giới tính:</label>
                  <div class="controls">Nam</div>
                </div>
              </div>
            </div>

            <div class="span6">
              <div class="form-horizontal detail-item">
                <div class="control-group">
                  <label class="control-label">Bộ môn:</label>
                  <div class="controls">Tên Bộ môn</div>
                </div>
                <div class="control-group">
                  <label class="control-label">Trạng thái:</label>
                  <div class="controls"><span class="badge badge-success">Đang hoạt động</span></div>
                </div>
              </div>
            </div>

          </div>
          <div class="widget-box">
        		<div class="widget-title">
        			<h5>LỊCH SỬ MƯỢN TRẢ THIẾT BỊ / PHÒNG CHỨC NĂNG</h5>
        		</div>
        		<div class="widget-content nopadding">
        			<table class="table table-bordered table-hover data-table">
			          <thead>
			            <tr>
			              <th>Mã phiếu mượn</th>
			              <th>Thầy (Cô)</th>
			              <th>Thiết bị / Phòng chức năng</th>
			              <th>Bài học</th>
			              <th>Lớp</th>
			              <th>Mượn lúc</th>
			              <th>Trả lúc</th>
			              <th>Trạng thái</th>
			            </tr>
			          </thead>

			          <tbody>
			            <?php for ($i=1; $i <= 10; $i++) { ?>
			              <tr>
			                <td class="center"><?php echo $i; ?></td>
			                <td>Nguyễn Văn A</td>
			                <td>
			                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small><br>
			                  <strong>Tên thiết bị dạy học</strong> <small><em>(1)</em></small><br>
			                  <strong>Tên thiết bị dạy học</strong> <small><em>(1)</em></small>
			                </td>
			                <td>Tên bài học</td>
			                <td class="text-center">12TL2</td>
			                <td class="center">9:00 - 01/01/2018</td>
			                <td class="center">9:45 - 01/01/2018</td>
			                <td class="center"><span class="badge badge-success">Đã trả</span> / <span class="badge badge-important">Chưa trả</span></td>
			              </tr>
			            <?php } ?>
			          </tbody>
			        </table>
        		</div>
        	</div>
        </div>
      </div>
    </div>

  </div>
</div>

<div id="popupThemGV" class="modal modal-lg hide">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>THÊM GIÁO VIÊN</h3>
  </div>
  <div class="modal-body">
    <form id="frmThemTK" class="row-fluid form-horizontal" style="margin-top: 0;">
      <div class="control-group">
        <label class="control-label">Mã đăng nhập:</label>
        <div class="controls">
          <input type="text" class="span10" name="MaDangNhap" placeholder="Nhập mã đăng nhập">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Mật khẩu mới:</label>
        <div class="controls">
          <input type="password" class="span10" name="MatKhau" placeholder="Nhập mật khẩu mới">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Nhập lại mật khẩu mới:</label>
        <div class="controls">
          <input type="password" class="span10" name="NhapLaiMatKhau" placeholder="Nhập lại mật khẩu mới">
        </div>
      </div>
    </form>
    <div id="ajaxLoading" class="ajaxLoading_them"></div>
    <div id="errThemTK" style="margin-left: 200px;"></div>
  </div>
  <div class="modal-footer">
    <a href="javascript:void(0);" class="btn btn-primary" onclick="xulyThemTK();return false;"><em class="fa fa-save"></em> Thêm</a>
    <a data-dismiss="modal" class="btn" href="#">Hủy</a>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail() {
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").hide();
  }
</script>