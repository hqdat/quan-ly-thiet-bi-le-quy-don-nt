<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-content nopadding">
        <form action="<?php echo base_url('danhmuc/bomon'); ?>" method="post" class="form-horizontal">
          <div class="control-group">
            <label class="control-label">Bộ môn mới :</label>
            <div class="controls">
              <input type="text" class="span4" name="BoMonMoi" placeholder="Nhập tên bộ môn mới" value="<?php echo $bomon; ?>">
              <button type="submit" class="btn btn-primary" name="do" value="1"><em class="fa fa-plus"> </em> Thêm</button>
              <?php if (!empty($error)): ?>
                <span class="text-error"><?php echo $error; ?></span>
              <?php endif ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH BỘ MÔN</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th width="100">#</th>
              <th>Tên bộ môn</th>
              <th>Số giáo viên</th>
              <th width="200">Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listBoMon as $item): ?>
              <tr>
                <td class="center"><?php echo $item['order']; ?></td>
                <td class="center"><?php echo $item['TenBM']; ?></td>
                <td class="center"><?php echo $item['SLGiaoVien']; ?></td>
                <td class="center">
                  <a href="#" class="btn btn-success btn-mini" onclick="showDetail('<?php echo $item['MaBM']; ?>','<?php echo $item['TenBM']; ?>');return false;"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini" onclick="xulyXoaBM('<?php echo $item['MaBM']; ?>','<?php echo $item['TenBM']; ?>');return false;"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <h3>CẬP NHẬT TÊN BỘ MÔN: <span id="TenBM_title"></span></h3>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form id="frmDoiTenBM" class="form-horizontal">
            <input type="hidden" name="MaBM" id="MaBM">
            <div class="control-group">
              <label class="control-label">Tên bộ môn:</label>
              <div class="controls">
                <input type="text" name="TenBM" id="TenBM_input" class="span6" placeholder="Nhập tên bộ môn">
              </div>
            </div>
            
            <div class="form-actions">
              <div id="ajaxLoading" class="pull-left ajaxLoading_update" style="margin-bottom: 15px;"></div>
              <div class="clearfix"></div>
              <div id="errDoiTenBM" style="margin-bottom: 15px;"></div>

              <button type="submit" class="btn btn-primary" onclick="xulyDoiTenBM();return false;"><em class="fa fa-save"> </em> Đổi tên bộ môn</button>
              <button type="submit" class="btn" onclick="closeDetail(); return false;"><em class="fa fa-angle-left"> </em> Hủy bỏ</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail(mabm, tenbm) {
    $('#TenBM_title').html(tenbm);
    $('#TenBM_input').val(tenbm);
    $('#MaBM').val(mabm);

    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").fadeOut(200);
  }

  var dangXuLy = false;
  function xulyDoiTenBM() {
    if (dangXuLy == false) {
      $(".ajaxLoading_update").show();
      dangXuLy = true;
      var frmData = $('#frmDoiTenBM').serialize();

      $.ajax({
          url : baseurl + 'danhmuc/xulyDoiTenBM',
          type : 'POST',
          data : frmData,
          dataType: 'json',
          success : function(res){
              $(".ajaxLoading_update").hide();
              dangXuLy = false;
              
              if (res.status == false) {
                  $('#errDoiTenBM').removeClass('text-success').addClass('text-error').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      $('#errDoiTenBM').slideUp(200);
                  }, 3000);
              } else {
                  $('#errDoiTenBM').removeClass('text-error').addClass('text-success').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      location.reload();
                  }, 700);
              }
          }
      });
    }
  }

  function xulyXoaBM(mabm, tenbm) {
    if (confirm("Bạn có chắc chắn muốn xóa bộ môn: " + tenbm + " ?")) {
      if (dangXuLy == false) {
        dangXuLy = true;
        $.ajax({
            url : baseurl + 'danhmuc/xulyXoaBM',
            type : 'POST',
            data : {MaBM : mabm},
            dataType: 'json',
            success : function(res){
                dangXuLy = false;
                
                if (res.status == false) {
                    alert(res.message);
                } else {
                    alert(res.message);
                    location.reload();
                }
            }
        });
      }
    }
  }
</script>