<div class="row-fluid">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-content">
        <form action="<?php echo base_url('danhmuc/phong'); ?>" method="post" class="form-horizontal">
          <div class="row-fluid" style="margin: 0">
            <div class="control-group span4" style="border: none">
              <label class="control-label">Loại:</label>
              <div class="controls">
                <select name="LoaiPhong" class="span11">
                  <?php foreach ($loaiPhong as $index => $name): ?>
                    <option value="<?php echo $index; ?>"<?php echo ($index == $loai ? ' selected' : ''); ?>><?php echo $name; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="control-group span8">
              <label class="control-label">Phòng mới:</label>
              <div class="controls">
                <input name="TenPhongMoi" type="text" class="span4" placeholder="Nhập tên phòng mới" value="<?php echo $phong; ?>">
                <button type="submit" class="btn btn-primary" name="do" value="1"><em class="fa fa-plus"> </em> Thêm</button>
                <?php if (!empty($error)): ?>
                  <span class="text-error"><?php echo $error; ?></span>
                <?php endif ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-list"></i></span> 
        <h5>DANH SÁCH PHÒNG CHỨC NĂNG</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th width="100">#</th>
              <th>Tên phòng</th>
              <th>Loại</th>
              <th width="200">Thao tác</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($listPhong as $item): ?>
              <tr>
                <td class="center"><?php echo $item['order']; ?></td>
                <td class="center"><?php echo $item['TenPhong']; ?></td>
                <td class="center"><?php echo $item['loai']; ?></td>
                <td class="center">
                  <a href="#" class="btn btn-success btn-mini" onclick="showDetail('<?php echo $item['order']; ?>','<?php echo $item['TenPhong']; ?>',<?php echo $item['LoaiPhong']; ?>);return false;"><em class="fa fa-pencil"> </em> Cập nhật</a>
                  <a href="#" class="btn btn-danger btn-mini" onclick="xulyXoaPhong('<?php echo $item['MaPhong']; ?>','<?php echo $item['TenPhong']; ?>');return false;"><em class="fa fa-trash"> </em> Xóa</a>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="ajaxLoading"></div>
    <div id="detail-area" style="display: none;">
      <hr>
      <h3>CẬP NHẬT PHÒNG: <span id="update_TenPhong"></span></h3>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form id="frmUpdatePhong" class="form-horizontal">
            <input type="hidden" name="MaPhong" id="update_MaPhong">
            <div class="control-group">
              <label class="control-label">Loại:</label>
              <div class="controls">
                <select name="LoaiPhong" id="update_LoaiPhong" class="span6">
                  <?php foreach ($loaiPhong as $index => $name): ?>
                    <option value="<?php echo $index; ?>"><?php echo $name; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Tên phòng:</label>
              <div class="controls">
                <input type="text" name="TenPhong" id="update_TenPhong_input" class="span6" placeholder="Nhập tên phòng">
              </div>
            </div>
            
            <div class="form-actions">
              <div id="ajaxLoading" class="pull-left ajaxloading_update" style="margin-bottom: 15px;"></div>
              <div class="clearfix"></div>
              <div id="errUpdate" style="margin-bottom: 15px;"></div>

              <button type="submit" class="btn btn-primary" onclick="xulyUpdatePhong();return false;"><em class="fa fa-save"> </em> Lưu</button>
              <button class="btn" onclick="closeDetail(); return false;"><em class="fa fa-angle-left"> </em> Hủy bỏ</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function scrollToDetail() {
    $('html, body').delay(500).animate({
        scrollTop: $("#detail-area").offset().top
    }, 500);
  }
  function showDetail(maphong, tenphong, loaiphong) {
    $("#update_TenPhong").html(tenphong);
    $("#update_TenPhong_input").val(tenphong);
    $("#update_LoaiPhong").val(loaiphong);
    $("#update_MaPhong").val(maphong);
    $('select').select2();
    $("#detail-area").slideUp(100);
    $("#ajaxLoading").show().delay(500).slideUp(100);
    $("#detail-area").slideDown(300).show();
    scrollToDetail();
  }
  function closeDetail() {
    $("#detail-area").fadeOut(200);
  }
  var dangXuLy = false;
  function xulyXoaPhong(maphong, tenphong) {
    if (confirm("Bạn có chắc chắn muốn xóa: " + tenphong + " ?")) {
      if (dangXuLy == false) {
        dangXuLy = true;
        $.ajax({
            url : baseurl + 'danhmuc/xulyXoaPhong',
            type : 'POST',
            data : {MaPhong : maphong},
            dataType: 'json',
            success : function(res){
                dangXuLy = false;
                
                if (res.status == false) {
                    alert(res.message);
                } else {
                    alert(res.message);
                    location.reload();
                }
            }
        });
      }
    }
  }

  function xulyUpdatePhong() {
    if (dangXuLy == false) {
      $(".ajaxloading_update").show();
      dangXuLy = true;
      var frmData = $('#frmUpdatePhong').serialize();

      $.ajax({
          url : baseurl + 'danhmuc/xulyUpdatePhong',
          type : 'POST',
          data : frmData,
          dataType: 'json',
          success : function(res){
              $(".ajaxloading_update").hide();
              dangXuLy = false;
              
              if (res.status == false) {
                  $('#errUpdate').removeClass('text-success').addClass('text-error').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      $('#errUpdate').slideUp(200);
                  }, 3000);
              } else {
                  $('#errUpdate').removeClass('text-error').addClass('text-success').html(res.message).slideDown(200);
                  
                  setTimeout(function(){
                      location.reload();
                  }, 1200);
              }
          }
      });
    }
  }
</script>