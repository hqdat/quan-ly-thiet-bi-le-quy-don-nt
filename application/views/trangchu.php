<div class="quick-actions_homepage <?php echo (isset($modHome) ? $modHome : ''); ?>">
  <ul class="quick-actions">
    <li> <a href="<?php echo base_url('muontra/muonthietbi'); ?>"> <i class="fa fa-file-o"></i> Lập phiếu mượn thiết bị</a> </li>
    <li> <a href="<?php echo base_url('muontra/muonphong'); ?>"> <i class="fa fa-file"></i> Lập phiếu mượn phòng</a> </li>
    <li> <a href="#"> <i class="fa fa-flag"></i> Báo hỏng</a> </li>
    <li> <a href="#"> <i class="fa fa-exclamation-triangle"></i> Báo mất</a> </li>
  </ul>
</div>
<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-flag"></i></span> 
        <h5>DANH SÁCH THIẾT BỊ VÀ PHÒNG CHỨC NĂNG ĐANG ĐƯỢC MƯỢN SỬ DỤNG</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th>Mã phiếu mượn</th>
              <th>Thầy (Cô)</th>
              <th>Thiết bị / Phòng chức năng</th>
              <th>Bài học</th>
              <th>Lớp</th>
              <th>Mượn lúc</th>
              <th>Dự kiến trả</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
              <tr>
                <td class="center">PM00000001</td>
                <td>Nguyễn Văn A</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả thiết bị</a></td>
              </tr>
              <tr>
                <td class="center">PM00000002</td>
                <td>Nguyễn Văn B</td>
                <td>
                  <strong>Phòng thực hành Hóa</strong>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả phòng</a></td>
              </tr>
              <tr>
                <td class="center">PM00000003</td>
                <td>Nguyễn Văn B</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả thiết bị</a></td>
              </tr>
              <tr>
                <td class="center">PM00000004</td>
                <td>Nguyễn Văn B</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả thiết bị</a></td>
              </tr>
              <tr>
                <td class="center">PM00000005</td>
                <td>Nguyễn Văn B</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả thiết bị</a></td>
              </tr>
              <tr>
                <td class="center">PM00000006</td>
                <td>Nguyễn Văn B</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#popupTraTB" data-toggle="modal" class="btn btn-success btn-mini tip-top" data-original-title="Trả thiết bị"><em class="fa fa-check"> </em> Trả thiết bị</a></td>
              </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="row-fluid">
	<div class="span12">
    <div class="widget-box">
      <div class="widget-title">
         <span class="icon"><i class="fa fa-file"></i></span> 
        <h5>DANH SÁCH ĐĂNG KÝ MƯỢN THIẾT BỊ VÀ PHÒNG CHỨC NĂNG</h5>
      </div>
      <div class="widget-content nopadding table-responsive">
        <table class="table table-bordered table-hover data-table">
          <thead>
            <tr>
              <th>Mã phiếu mượn</th>
              <th>Thầy (Cô)</th>
              <th>Thiết bị / Phòng chức năng</th>
              <th>Bài học</th>
              <th>Lớp</th>
              <th>Dự kiến mượn</th>
              <th>Dự kiến trả</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            <?php for ($i=1; $i <= 15; $i++) { ?>
              <tr class="gradeX">
                <td class="center"><?php echo $i; ?></td>
                <td>Nguyễn Văn A</td>
                <td>
                  <strong>Tên thiết bị dạy học</strong> <small><em>(2)</em></small>
                </td>
                <td>Tên bài học</td>
                <td class="text-center">12TL2</td>
                <td class="center">9:00 - 01/01/2018</td>
                <td class="center">9:45 - 01/01/2018</td>
                <td class="center"><a href="#" class="btn btn-success btn-mini tip-top" data-original-title="Xác nhận phiếu mượn"><em class="fa fa-check"> </em> Xác nhận</a></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div id="popupTraTB2" class="modal modal-lg hide">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Alert modal</h3>
  </div>
  <div class="modal-body">
    <p>Lorem ipsum dolor sit amet...</p>
  </div>
  <div class="modal-footer"> <a data-dismiss="modal" class="btn btn-primary" href="#">Confirm</a> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
</div>