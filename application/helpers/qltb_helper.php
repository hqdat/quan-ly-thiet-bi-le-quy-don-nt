<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function vn_str_filter ($str){
    $unicode = array(
        'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
        'd' => 'đ',
        'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'i' => 'í|ì|ỉ|ĩ|ị',
        'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
		'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'D' => 'Đ',
        'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
        'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );
    
   foreach($unicode as $nonUnicode => $uni){
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
   }
	return $str;
}

function hoTen2MaDN ($hoten) {
	$hoten = strtolower(vn_str_filter($hoten));
	$hoten = explode(' ', $hoten);
	$rs = '';
	foreach ($hoten as $w) {
		$rs.= $w[0];
	}
	$rs = substr($rs, 0, -1);
	$rs = end($hoten) . '.' . $rs;
	return $rs;
}

function getBoMonById ($id) {
	$CI = get_instance();
    $CI->load->model('Model_Danhmuc');
    $bm = $CI->Model_Danhmuc->getBoMon($id);
    return $bm['TenBM'];
}

function getGVByMaDN ($madn) {
	$CI = get_instance();
    $CI->load->model('Model_Danhmuc');
    $gv = $CI->Model_Danhmuc->getGVByMaDN($madn);
    return $gv['HoTenGV'];
}

function numGVInBoMon ($bm) {
	$CI = get_instance();
    $CI->load->model('Model_Danhmuc');
    return $CI->Model_Danhmuc->numGVInBoMon($bm);
}

function newMaGV ($max) {
	if ($max < 10) {
		return 'GV00' . $max;
	} elseif ($max < 100) {
		return 'GV0' . $max;
	}
	return 'GV' . $max;
}

function getNHDauTien() {
	if (date('m') >= 8) return date('Y') . ' - ' . (date('Y') + 1);
	return (date('Y') - 1) . ' - ' . date('Y');
}

function listKhoiLop () {
	return [
		'1' => 'Khối 10',
		'2' => 'Khối 11',
		'3' => 'Khối 12'
	];
}

function listLoaiPhong() {
	return [
		'1' => 'Phòng thực hành',
		'2' => 'Phòng giáo án điện tử'
	];
}